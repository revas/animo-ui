import types from './animo.store.mutations'

const createAnimoStore = ($http) => {
  return {
    namespaced: true,
    state: {
      isWaiting: false
    },
    getters: {
      isWaiting: (state) => {
        return state.isWaiting
      }
    },
    mutations: {
      [types.ANIMO_EDIT_PROFILE_REQUEST] (state) {
        state.isWaiting = true
      },
      [types.ANIMO_EDIT_PROFILE_SUCCESS] (state) {
        state.isWaiting = false
      },
      [types.ANIMO_EDIT_PROFILE_FAILURE] (state, exception) {
        state.isWaiting = false
      }
    },
    actions: {
      async updateProfile ({ commit, dispatch }, profile) {
        commit(types.ANIMO_EDIT_PROFILE_REQUEST)
        try {
          const response = await $http.post('/profiles/animo.UpdateProfiles/', {
            profilesAliases: ['me'],
            profiles: [profile]
          })
          console.log(response)
          commit(types.ANIMO_EDIT_PROFILE_SUCCESS)
          dispatch('app/getProfile', null, {root: true})
        } catch (e) {
          console.log(e)
          commit(types.ANIMO_EDIT_PROFILE_FAILURE)
        }
      }
    }
  }
}

export default createAnimoStore
