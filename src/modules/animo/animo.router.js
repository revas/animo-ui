import VProfileEditor from '@/modules/animo/views/VProfileEditor'
import VProfileViewer from '@/modules/animo/views/VProfileViewer'

export default [
  {
    path: 'edit',
    name: 'ANIMO.PROFILE_EDITOR.$NAME',
    component: VProfileEditor
  },
  {
    path: 'view',
    name: 'ANIMO.PROFILE_VIEWER.$NAME',
    component: VProfileViewer
  }
]
